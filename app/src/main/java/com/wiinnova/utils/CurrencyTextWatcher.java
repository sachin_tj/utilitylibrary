package com.wiinnova.utils;

import java.text.NumberFormat;

import android.text.Editable;
import android.text.TextWatcher;

public class CurrencyTextWatcher implements TextWatcher {

    boolean mEditing;

    public CurrencyTextWatcher() {
        mEditing = false;
    }

    public synchronized void afterTextChanged(Editable s) {
        if(!mEditing) {
            mEditing = true;

            String digits = s.toString().replaceAll("\\D", "");
//            NumberFormat nf = NumberFormat.getCurrencyInstance();
            NumberFormat nf = NumberFormat.getInstance();
            nf.setGroupingUsed(true);
            try{
//                String formatted = nf.format(Double.parseDouble(digits)/100);
            	String formatted = nf.format(Double.parseDouble(digits));
                s.replace(0, s.length(), formatted);
            } catch (NumberFormatException nfe) {
                s.clear();
            }

            mEditing = false;
        }
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

    public void onTextChanged(CharSequence s, int start, int before, int count) { }

}
