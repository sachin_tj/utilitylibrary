package com.wiinnova.utils;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;


public class Debug extends Application{
	
	static final boolean production = false;
	
	public static void showLog(String context, String message){
		
		if (!production){
			if (context == null)
				context = "NULL";
			if (message == null)
				message = "NULL";
			Log.e(context, message);
		}
	}
	public static void showLog(String message){
		if (!production){
			if (message == null)
				message = "NULL";
			Log.e("-------------LOG----------------", message);
		}
	}
	public static void showToast(Context context, String message){
		if (!production){
			if (message == null)
				message = "NULL";
			Toast.makeText( context, message, Toast.LENGTH_LONG).show();
		}
	}
	
	public static boolean isProduction(){
		return production;
	}
}
