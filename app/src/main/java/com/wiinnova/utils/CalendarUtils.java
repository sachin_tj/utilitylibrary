package com.wiinnova.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;

@SuppressLint("NewApi")
public class CalendarUtils {

	public static String log = "";
	private static String ownerAccount;
	private static final String CALENDAR_ACCOUNT = "Dubai Chamber";
	private static final String CALENDAR_DISPLAY_NAME = "Dubai Chamber Calendar";

	public static String convertToHumanReadable (long milliseconds){
		Calendar today = Calendar.getInstance();
		Calendar postedDay = Calendar.getInstance();
		postedDay.setTimeInMillis(milliseconds);
		long day = 86400000L;
		long hour = 3600000L;
		long minute = 60000L;
		long month = 2628000000L;
		long year = 31536000000L;
		int time;

		if (today.getTimeInMillis() - postedDay.getTimeInMillis() > year) {
			time = Math.round((today.getTimeInMillis() - postedDay.getTimeInMillis()) / year);
			return time + (time == 1 ? " year ago" : " years ago");
		}
		else if (today.getTimeInMillis() - postedDay.getTimeInMillis() > month) {
			time = Math.round((today.getTimeInMillis() - postedDay.getTimeInMillis()) / month);
			return time + (time == 1 ? " month ago" : " months ago");
		}
		else if (today.getTimeInMillis() - postedDay.getTimeInMillis() > day) {
			time = Math.round((today.getTimeInMillis() - postedDay.getTimeInMillis()) / day);
			return time + (time == 1 ? " day ago" : " days ago");
		} else if (today.getTimeInMillis() - postedDay.getTimeInMillis() > hour) {
			time = Math.round((today.getTimeInMillis() - postedDay.getTimeInMillis()) / hour);
			return time + (time == 1 ? " hour ago" : " hours ago");
		} else {
			time = Math.round((today.getTimeInMillis() - postedDay.getTimeInMillis()) / minute);
			return time + (time == 1 ? " minute ago" : " minutes ago");
		}
	}

	public static String convertHrsToAmPm(int hours, int minutes){
		String time = null;
		if (hours == 0 && minutes < 10){
			time = String.valueOf(12)+":0"+String.valueOf(minutes)+ " AM";
		}
		else if (hours == 0 && minutes >= 10){
			time = String.valueOf(12)+":"+String.valueOf(minutes)+ " AM";
		}
		else if (hours > 12 && minutes < 10){
			time = String.valueOf(hours - 12)+":0"+String.valueOf(minutes)+ " PM";
		}
		else if (hours < 12 && minutes >= 10){
			time = String.valueOf(hours)+":"+String.valueOf(minutes)+ " AM";
		}
		else if (hours < 12 && minutes < 10){
			time = String.valueOf(hours)+":0"+String.valueOf(minutes)+ " AM";
		}
		else if (hours > 12 && minutes >= 10){
			time = String.valueOf(hours - 12)+":"+String.valueOf(minutes)+ " PM";
		}
		else if (hours == 12 && minutes < 10){
			time = String.valueOf(12)+":0"+String.valueOf(minutes)+ " PM";
		}
		else if (hours == 12 && minutes >= 10){
			time = String.valueOf(12)+":"+String.valueOf(minutes)+ " PM";
		}

		return time;
	}

	public static String adjustTimeDigits(int hours, int minutes){
		String time = null;
		if (hours < 10 && minutes < 10)
			time = "0"+hours+":"+"0"+minutes;
		else if (hours < 10)
			time = "0"+hours+":"+minutes;
		else if (minutes < 10)
			time = hours+":"+"0"+minutes;
		else
			time = hours +":"+minutes;
		return time;
	}

	public static String adjustDateDigits(int day, int month, int year){
		String time = null;
		if (day < 10 && month < 10)
			time = "0"+day+"-"+"0"+month+"-"+year;
		else if (day < 10)
			time = "0"+day+"-"+month+"-"+year;
		else if (month < 10)
			time = day+"-"+"0"+month+"-"+year;
		else
			time = day+"-"+month+"-"+year;
		return time;
	}

	public static void addEventToMyCalendar(Context context, String title, String comment, long startTime){
		if (!myCalendarExists(context)){
			addMyCalendar(context);
		}
		Cursor c = getMyCalendar(context);
		if ( c != null && c.getCount() > 0){
			c.moveToFirst();
			ContentResolver cr = context.getContentResolver();
			ContentValues values = new ContentValues();

			values.put(CalendarContract.Events.DTSTART, startTime);
			values.put(CalendarContract.Events.TITLE, title);
			values.put(CalendarContract.Events.DESCRIPTION, comment);

			TimeZone timeZone = TimeZone.getDefault();
			values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());

			// default calendar
			values.put(CalendarContract.Events.CALENDAR_ID, getMyCalendarId(c));

			//        values.put(CalendarContract.Events.RRULE, "FREQ=DAILY;UNTIL="
			//                + dtUntill);
			//for one hour
			values.put(CalendarContract.Events.DURATION, "+P1H");

			values.put(CalendarContract.Events.HAS_ALARM, 1);

			// insert event to calendar
			//	        Uri baseUri;
			//	        if (Build.VERSION.SDK_INT >= 8) {
			//		        baseUri = Uri.parse("content://com.android.calendar/events");
			//		    } else {
			//		        baseUri = Uri.parse("content://calendar/events");
			//		    }

			cr.insert(CalendarContract.Events.CONTENT_URI, values);
		}

	}


	public static void deleteCalendarEvent(Context context, String eventId){
		Cursor cursor = context.getContentResolver().query(CalendarContract.Events.CONTENT_URI,
				new String[]{CalendarContract.Events._ID}, CalendarContract.Events._ID+" = " + eventId, null, null);  

		while(cursor.moveToNext()) {
			Uri deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, cursor.getInt(0));
			int result = context.getContentResolver().delete(deleteUri, null, null);
			//		    UserInterface.showToast(context, result+ " "+ "event deleted");
		}
	}


	private static long getMyCalendarId(Cursor c) {
		// TODO Auto-generated method stub
		return c.getLong(0);
	}

	public static String getUpdatedAt() {
		// TODO Auto-generated method stub
		Calendar c = Calendar.getInstance();

		return c.get(Calendar.YEAR) + "-" +(c.get(Calendar.MONTH) + 1) + "-" +c.get(Calendar.DAY_OF_MONTH) 
				+ " " +c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE)+":"+ c.get(Calendar.SECOND);
	}

	public static void addMyCalendar(Context context){
		Account myAccount = getAccount(context);
		if (myAccount != null){
			ownerAccount = myAccount.name;
			if (Build.VERSION.SDK_INT > 14){
				if (!myCalendarExists(context)){
					log += "Creating " + CALENDAR_DISPLAY_NAME;
					Uri calUri = CalendarContract.Calendars.CONTENT_URI;
					ContentValues cv = new ContentValues();
					cv.put(CalendarContract.Calendars.NAME, CALENDAR_DISPLAY_NAME);
					cv.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, CALENDAR_DISPLAY_NAME);
					cv.put(CalendarContract.Calendars.VISIBLE, 1);
					cv.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
					cv.put(CalendarContract.Calendars.ACCOUNT_NAME, CALENDAR_ACCOUNT);
					cv.put(CalendarContract.Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL);
					cv.put(CalendarContract.Calendars.CALENDAR_COLOR, 14417920);
					cv.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL, 700);
					cv.put(CalendarContract.Calendars.OWNER_ACCOUNT, ownerAccount);

					calUri = calUri.buildUpon()
							.appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
							.appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, CALENDAR_ACCOUNT)
							.appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL)
							.build();
					Uri result = context.getContentResolver().insert(calUri, cv);
					//			    Debug.showToast(context, "Add Calndar Result" + result.toString());
					log+= "Add Calndar Result" + result.toString() + "\n";
				}

			}
		}
	}

	private static Account getAccount(Context context) {
		// TODO Auto-generated method stub
		Account[] accounts = AccountManager.get(context).getAccounts();
		Account myAccount = null;
		for (Account a : accounts){
			if (a.type.contains("com.google")){
				//				Debug.showToast(context, a.name + " " + a.type);
				myAccount = a;
				break;
			}
		}
		return myAccount;
	}

	public static Cursor getMyCalendar(Context context){
		log += "----------Calendars Found---------------";
		ownerAccount = getAccount(context) != null ? getAccount(context).name : null;
		if (Build.VERSION.SDK_INT > 14 ) {

			// Projection array. Creating indices for this array instead of doing
			// dynamic lookups improves performance.
			final String[] EVENT_PROJECTION = new String[] {
					Calendars._ID,                           // 0
					Calendars.ACCOUNT_NAME,                  // 1
					Calendars.CALENDAR_DISPLAY_NAME,         // 2
					Calendars.OWNER_ACCOUNT                  // 3
			};

			// The indices for the projection array above.
			final int PROJECTION_ID_INDEX = 0;
			final int PROJECTION_ACCOUNT_NAME_INDEX = 1;
			final int PROJECTION_DISPLAY_NAME_INDEX = 2;
			final int PROJECTION_OWNER_ACCOUNT_INDEX = 3;

			// Run query
			Cursor cur = null;
			ContentResolver cr = context.getContentResolver();
			Uri uri = Calendars.CONTENT_URI;   
			String selection = "((" + Calendars.ACCOUNT_NAME + " = ?) AND (" 
					+ Calendars.ACCOUNT_TYPE + " = ?) AND ("
					+ Calendars.OWNER_ACCOUNT + " = ?))";
			String[] selectionArgs = new String[] {CALENDAR_ACCOUNT, CalendarContract.ACCOUNT_TYPE_LOCAL, ownerAccount }; 
			// Submit the query and get a Cursor object back. 
			cur = cr.query(uri, EVENT_PROJECTION, selection, selectionArgs, null);
			//			cur.moveToFirst();
			// Use the cursor to step through the returned records
			while (cur.moveToNext()) {
				long calID = 0;
				String displayName = null;
				String accountName = null;
				String ownerName = null;

				// Get the field values
				calID = cur.getLong(PROJECTION_ID_INDEX);
				displayName = cur.getString(PROJECTION_DISPLAY_NAME_INDEX);
				accountName = cur.getString(PROJECTION_ACCOUNT_NAME_INDEX);
				ownerName = cur.getString(PROJECTION_OWNER_ACCOUNT_INDEX);

				// Do something with the values...
				log += "ID : "+calID + " Display Name : "+ displayName +" Account Name : " + accountName
						+ " Owner Name " + ownerName +" \n";
			}

			//			saveLog(context);

			return cur;
		}
		return null;
	}

	public static boolean myCalendarExists(Context context){
		Cursor c = getMyCalendar(context);
		if (c != null && c.getCount() > 0){
			log += CALENDAR_DISPLAY_NAME +" exists";
			return true;
		}
		log += CALENDAR_DISPLAY_NAME +" does not exist";
		return false;
	}

	public static void saveLog(Context context){
		if (Build.VERSION.SDK_INT > 19){
			try{
				File f = new File(Environment.getExternalStorageDirectory(), "DC_LOG.txt");

				FileWriter fw = new FileWriter(f);
				fw.append(log);
				fw.flush();
				fw.close();
				Debug.showToast(context, "Log Saved");
				//				log = "";
			}
			catch (IOException ie){
				ie.printStackTrace();
			}
		}

	}

}

