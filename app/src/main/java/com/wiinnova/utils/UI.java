package com.wiinnova.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

public class UI {

	private static ProgressDialog pg;
	public static void showOKAlert(Context c, String message, DialogInterface.OnClickListener listener ) {
		// TODO Auto-generated constructor stub
		if ( c != null){
			AlertDialog.Builder b = new Builder(c);
			b.setTitle("Info");
			b.setMessage(message);
			b.setPositiveButton("OK", listener);
			AlertDialog d = b.create();
			d.show();
		}
	}
	
	public static void showOkCancelAlert(Context c, String message, DialogInterface.OnClickListener okListener) {
		// TODO Auto-generated constructor stub
		if ( c != null){
			AlertDialog.Builder b = new Builder(c);
			b.setTitle("Info");
			b.setMessage(message);
			b.setPositiveButton("OK", okListener);
			b.setNegativeButton("Cancel", null);
			AlertDialog d = b.create();
			d.show();
		}
	}
	
	public static void showToast(Context context, String message){
		if (context != null)
			Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}
	
	public static void hideKeyboard(Context context, View view) {
		InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	public static void makeActionOverflowMenuShown(Context context) {
	    //devices with hardware menu button (e.g. Samsung Note) don't show action overflow menu
	    try {
	        ViewConfiguration config = ViewConfiguration.get(context);
	        java.lang.reflect.Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
	        if (menuKeyField != null) {
	            menuKeyField.setAccessible(true);
	            menuKeyField.setBoolean(config, false);
	        }
	    } catch (Exception e) {
	        Log.d("Overflow menu show error", e.getLocalizedMessage());
	    }
	}
	public static void showProgress(Context c, String message){
		pg = ProgressDialog.show(c, "", message, true, true);
	}
	public static void showProgress(Context c){
		hideProgress();
		try{
		if (c != null)
			pg = ProgressDialog.show(c, "", "Loading...", true, true);
		}
		catch(Exception e){
			
		}
	}
	public static void hideProgress(){
		try{
			if (pg != null && pg.isShowing())
				pg.cancel();
		}
		catch (Exception e){

		}
	}
}
