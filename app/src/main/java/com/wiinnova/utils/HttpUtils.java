package com.wiinnova.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpUtils {

	public static String httpGet(String url, String token) {
		HttpClient httpclient = new DefaultHttpClient();

		HttpGet httpGet = new HttpGet(url);
		httpGet.addHeader("X-Auth-Token", token);
		try {

			// Execute HTTP GET Request
			HttpResponse response = httpclient.execute(httpGet);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(), "UTF-8"));
			return reader.readLine();

		} catch (ClientProtocolException e) {
			e.printStackTrace();
			// TODO Auto-generated catch block
		} catch (IOException e) {
			e.printStackTrace();
			// TODO Auto-generated catch block
		}

		return null;
	}
	
	public static String postData(String url, String token, List<NameValuePair> args) {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		if (token != null)
			httppost.addHeader("X-Auth-Token", token);

		try {
			// Add your data
			
			if (args != null)
				httppost.setEntity(new UrlEncodedFormEntity(args));
			
			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
			InputStream is = response.getEntity().getContent();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append((line + "\n"));
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			return String.valueOf(sb);

		} catch (ClientProtocolException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (OutOfMemoryError oe) {
			oe.printStackTrace();
		}

		return null;
	}

	public static String postMultiPartData(String url, String token, ContentBody foto) {
	    // Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost(url);
	    
	    if (token != null)
			httppost.addHeader("X-Auth-Token", token);

	    try {
	        // Add your data
	        
//	        if (args != null){
        		/*MultipartEntity entity = new MultipartEntity();
        		entity.addPart("data", new StringBody(args.toString()));
        		//entity.addPart("image", new FileBody(photo));
        		entity.addPart("image", foto);
        		httppost.setEntity(entity); */
	        	
	        	MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
	        	if (foto != null)
	        		multipartEntity.addPart("image", foto);
//	        	StringBody sb = new StringBody(args.toString());
//	        	multipartEntity.addPart("data", sb );
	        	httppost.setEntity(multipartEntity.build());
//	        } 
	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        InputStream is = response.getEntity().getContent();
	        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	        StringBuilder sb = new StringBuilder();
	        String line = null;
	        try {
	            while ((line = reader.readLine()) != null) {
	                sb.append((line + "\n"));
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                is.close(); 
	            } catch (IOException e) {
	                e.printStackTrace(); 
	            }
	        }
	        
	        return String.valueOf(sb);
	        
	        
	    } catch (ClientProtocolException e) {
	    	e.printStackTrace();
	        // TODO Auto-generated catch block
	    } catch (IOException e) {
	    	e.printStackTrace();
	        // TODO Auto-generated catch block
	    }
	    catch (Exception ex){
	    	ex.printStackTrace();
	    }
	    catch (OutOfMemoryError oe) {
			// TODO: handle exception
	    	oe.printStackTrace();
		}
	    
	    return null;
	} 
}
